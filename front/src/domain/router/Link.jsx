// import { NavigationEvents } from "../events/NavigationEvents.js"

import { NavigationEvents } from "../events/NagivationEvents.js"

export const redirect = (href) => {
  window.history.pushState({}, '', href)
  const navigationEvent = new Event(NavigationEvents.navigate)
  window.dispatchEvent(navigationEvent)
}

export function Link({ target, to, ...props}) {
  const handleClick = (event) => {
    event.preventDefault()

    const isMainEvent = event.button === 0
    const isModifiedEvent = event.metaKey || event.altKey || event.ctrlKey || event.shiftKey
    const isManageableEvent = target === undefined || target === 'self'

    if (isMainEvent && isModifiedEvent && !isManageableEvent) {redirect(to)}
  }

  return <a onClick={handleClick} href={to} target={target} {...props}></a>
}