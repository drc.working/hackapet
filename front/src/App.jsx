import { Router, routes } from './domain/router/Router'

import './App.css'

function App() {
  return (
    <>
      <Router routes={routes}/>
    </>
  )
}

export default App
