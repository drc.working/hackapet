from django.db import models # type: ignore
# from . import User
import uuid

class User(models.Model):
    name = models.CharField(max_length=100)
    google_id = models.CharField(max_length=255)
    admin = models.BooleanField(default=False)
    sign = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)

    def __str__(self):
        return self.name


class Item(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    quantity = models.IntegerField()
    owner = models.ForeignKey(User, to_field='sign', on_delete=models.CASCADE, related_name='items')

    def __str__(self):
        return self.name