from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin #type: ignore
from django.core.exceptions import PermissionDenied

from api.authenticators.token_authenticator import TokenAuthenticator

import sys, json

class SessionMiddleware(MiddlewareMixin):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        return response

    def process_response(self, request):
        google_cookie = request.COOKIES.get('g_state')
        session_cookie = request.COOKIES.get('session_token')

        session_token = TokenAuthenticator.decode_token(session_cookie)

        tokens = {google_cookie, session_token}


        if google_cookie == '{"i_l":0}' or session_token['ok'] != False:
        # if '{"i_l": 0}' == '{`i_l`: 0}':
            # print('TOKENS: ', tokens, file=sys.stderr)
            return self.get_response
        else:
            raise PermissionDenied('Invalid token')
            return self.get_response