class SessionHolder:
    user = {}

    @staticmethod
    def set_user(credentials):
        SessionHolder.user = credentials

    @staticmethod
    def get_user():
        return SessionHolder.user