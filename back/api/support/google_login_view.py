from django.http import JsonResponse # type: ignore
from api.models import User

from google.oauth2 import id_token
from google.auth.transport import requests

from api.support.session_holder import SessionHolder

import sys, json, uuid

class GoogleLoginView:
    @staticmethod
    def create_response(request):
        ID = '312584381594-tr6j8k85iojnajba3daa6jb58fdg901b.apps.googleusercontent.com'
        body_unicode = request.body.decode('utf-8')
        body_data = json.loads(body_unicode)
        token = body_data.get('token', None)

        if not token: return JsonResponse({'error': 'bad request'}, status=400)

        try:
            decoded_token = id_token.verify_oauth2_token(token, requests.Request(), ID)
        except ValueError:
            return JsonResponse({'error': 'Invalid token'}, status=401)

        user = User.objects.filter(google_id=decoded_token['sub'])

        if not user:
            new_user = User(
                name = decoded_token['given_name'],
                google_id = decoded_token['sub'],
                admin = False,
                sign = uuid.uuid4()
            )
            new_user.save()

        logged_user = User.objects.filter(google_id=decoded_token['sub']).values()[0]
        credentials = {
            'name': logged_user['name'],
            'sign': str(logged_user['sign'])
        }

        SessionHolder.set_user(credentials)
        print('[BACK] users', credentials, file=sys.stderr)
        return JsonResponse({
            'name': logged_user['name'],
            'sign': str(logged_user['sign'])
        })