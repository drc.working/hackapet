import { requester } from "../domain/Requester"

const sampleItem = {
    name: 'Arduino 3',
    description: 'A working blue arduino',
    quantity: 1,
    owner: '625dd60f-084b-4e5f-b8e9-3eb9bbdcfc26'
}

function handleSubmit(event) {
    event.preventDefault()

    const fields = Object.fromEntries(new FormData(event.target))
    const url = `http://localhost:8000/v1/create_item/`
    const payload = sampleItem
    requester.post({url, payload})
}

export function AddItem({ owner }) {

    return (

        <form onSubmit={handleSubmit}>
            <input type="text" placeholder="name"/>
            <input type="text" placeholder="name"/>
            <input type="text" placeholder="name"/>
            <button type="submit">Submit</button>
        </form>
        // <section>
        //     <button>Add Item</button>
        // </section>
    )
}