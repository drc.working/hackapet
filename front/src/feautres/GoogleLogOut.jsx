import { redirect } from '../domain/router/Link';
import { GoogleManager } from '../domain/events/google_session/GoogleManager'

function handleLogout() {
  GoogleManager.logOut()
  redirect('/')
};

export function GoogleLogOut() {

  return (
    <button
      onClick={handleLogout}
    >
      Log Out
    </button>
  )
}