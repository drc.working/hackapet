from django.test import TestCase, Client # type: ignore

import json
class TestRequester(TestCase):
  c = Client()
  login_url = '/v1/login/'
  users_url = '/v1/users/'
  item_url = '/v1/create_item/'

  def test_saves_sample_item(self):
    data = {
      'name': 'arduino',
      'description': 'A fine blue arduino',
      'quantity': 1,
      'owner': '625dd60f-084b-4e5f-b8e9-3eb9bbdcfc26'
    }
    payload = json.dumps(data)
    response = self.c.post(self.item_url, data, content_type='application/json')
    parsed = json.loads(response.content.decode('utf-8'))

    self.assertEqual(parsed, True)