import { useEffect } from 'react';
import { GoogleManager } from '../domain/events/google_session/GoogleManager';

export function GoogleLogin () {

    useEffect(() => {
        GoogleManager.initialize()
    }, []);

    return (
        <div
            id="google-signin-button"
        />
    );
};