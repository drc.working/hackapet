from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied

import jwt

class TokenAuthenticator:
    @staticmethod
    def generate_token(user):
        payload = {
            'user_name': user,
            'ok': True,
        }
        token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256')
        return token

    @staticmethod
    def decode_token(token):
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            return payload['user_name']

        except jwt.ExpiredSignatureError:
            raise PermissionDenied('Token has expired')
        except jwt.InvalidTokenError:
            raise PermissionDenied('Invalid token')

    @staticmethod
    def authenticate(request):
        auth_header = request.COOKIES.get('session_token')
        if not auth_header:
            raise PermissionDenied('No token provided')

        try:
            token = auth_header.split(' ')[1]
            user_name = TokenAuthenticator.decode_token(token)
            user = User.objects.get(name=user_name)
            return user
        except IndexError:
            raise PermissionDenied('Token format invalid')
        except User.DoesNotExist:
            raise PermissionDenied('User not found')
