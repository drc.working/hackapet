export class Requester {

  async get({url}) {
    return await fetch(url, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'content-type': 'application/json',
        'Access-Control-Allow-Credentials': true
      }
    })
    .then( response => {
      if (!response.ok) {
        console.log('[FRONT] Bad request')
      }

      return response.json()
    })
  }

  async post({url, payload}) {
    return await fetch(url, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-type': 'application/json',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(payload)
    })
    .then( response => {
      if (!response.ok) {
        // console.log('--------------', JSON.stringify(payload))
       return console.log('[FRONT] Bad request')
      }

      return response.json().credentials
    })
  }
}

export const requester = new Requester()