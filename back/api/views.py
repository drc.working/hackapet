from django.shortcuts import HttpResponse # type: ignore
from django.http import JsonResponse # type: ignore
from rest_framework.decorators import api_view # type: ignore

from api.models import Item, User

from google.oauth2 import id_token
from google.auth.transport import requests

from api.support.login_view import LoginView
from api.support.google_login_view import GoogleLoginView
from api.support.session_holder import SessionHolder

import json, sys, uuid

@api_view(['GET'])
def index(request):

    return HttpResponse("<h1>Hello World</h1>")

@api_view(['GET'])
def users(request):
    user = SessionHolder.get_user()

    return JsonResponse(user)

@api_view(['GET'])
def inventory(request):
    user = SessionHolder.get_user()
    sign = user['sign']
    # print('****************', user['sign'], file=sys.stderr)

    filtered_items = Item.objects.filter(owner__sign=sign).values()
    item_list = list(filtered_items.values())


    if not item_list:
        list = []
    # serialized = json.dumps(list)
    print('----------------', list, file=sys.stderr)

    return JsonResponse(json.dumps(list), safe=False)

@api_view(['POST'])
def login(request):

    return LoginView.create_response(request)

@api_view(['POST'])
def google_login(request):
    return GoogleLoginView.create_response(request)

@api_view(['POST'])
def create_item(request):
    body_unicode = request.body.decode('utf-8')
    payload = json.loads(body_unicode)
    name, description, quantity, owner = payload
    print('#$#$#$#$#$#$#$', payload, file=sys.stderr)
    # serialiez_uuid=uuid.UUID(owner)
    # user = User.objects.filter(sign=serialiez_uuid)
    # new_item = Item(name=name, description=description, quantity=quantity, owner=user)
    # new_item.save()
    return JsonResponse(payload)