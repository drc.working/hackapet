import { requester } from "../domain/Requester"
import { redirect } from "../domain/router/Link"
import { GoogleLogin } from "./GoogleLogin"

async function handleLogin(event) {
    event.preventDefault()

    const data = Object.fromEntries(new window.FormData(event.target))
    const url = 'http://localhost:8000/v1/login/'

    const token = await requester.post({url, data})

    if (token.ok) { return redirect('/inventory') }

    return
}

export function LogIn () {

  return (
    <main>
      <h1>Log in</h1>

      <form onSubmit={handleLogin}>
        <input name='user_name' type="text" placeholder='email' defaultValue='diego'/>
        <input name='password' type="text" placeholder='password' defaultValue='123'/>
        <button type="submit">Log in</button>
      </form>

      <GoogleLogin></GoogleLogin>
    </main>
  )
}