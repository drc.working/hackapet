import { redirect } from "../../router/Link";

export class GoogleManager {

  static initialize() {
    const CLIENT_ID = '312584381594-tr6j8k85iojnajba3daa6jb58fdg901b.apps.googleusercontent.com'

    window.google.accounts.id.initialize({
      client_id: CLIENT_ID,
      callback: this.createResponse,
    });

    window.google.accounts.id.renderButton(
      document.getElementById('google-signin-button'),
      { theme: 'outline', size: 'large' }
    );

    window.google.accounts.id.prompt()
  }

  static logOut() {
    google.accounts.id.disableAutoSelect()
  }

  static async createResponse(response) {
    const BACK_URL = 'http://localhost:8000/v1/rest-auth/google/'

    fetch(BACK_URL, {
        method: 'POST',
        headers: {
        // 'credentials': 'include',
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({ token: response.credential }),
    })
    .then(res => {

      if (res.ok) {
        const data = res.json()
        redirect('/inventory')
        return data
      } else {
        redirect('/')
      }

    })
    .catch(error => {
        console.error('Error logging in with Google', error);
    });
    }
}