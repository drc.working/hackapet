export const NavigationEvents = {
  navigate: 'pushstate',
  popstate: 'popstate'
}