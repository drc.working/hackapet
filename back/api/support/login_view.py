from django.http import JsonResponse

from src.users_repository import UsersRepository
from api.authenticators.token_authenticator import TokenAuthenticator

import json, sys

class LoginView(JsonResponse):
  repository = UsersRepository()

  @staticmethod
  def match_user(credentials):
    founded_user = False
    accounts = LoginView.repository.get_all()

    user_string = json.dumps(credentials)

    for account in accounts:
        pattern = json.dumps(account)

        if user_string == pattern:
            founded_user = True
            return founded_user

    return founded_user

  @staticmethod
  def create_response(request):
    user = {
        "user_name": request.data['user_name'],
        "password": request.data['password']
    }

    founded_user = LoginView.match_user(user)

    if founded_user:
      # print(f'[BACK] Requested data: {user['user_name']}', file=sys.stderr)

      token = TokenAuthenticator.generate_token(user['user_name'])
      print(f"[BACK] Token created: {token}", file=sys.stderr)

      response = JsonResponse({'200': 'token created'})
      response.set_cookie(
              key='session_token',
              value=token,
              # max_age=3600,  # 1 hour
              httponly=True,  # Prevent JavaScript access to the cookie
              secure=False,  # Only send cookie over HTTPS
              samesite='Lax')

      response.status_code = 200

      return response
    else:
      print('[BACK] Access Denied')
      return JsonResponse({'Error': 'Invalid Credentials'}, status=401)