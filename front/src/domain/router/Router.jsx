import { useEffect, useState } from "react"
import { NavigationEvents } from "../events/NagivationEvents"
import { LogIn } from "../../feautres/LogIn"
import { Inventory } from "../../feautres/Inventory"
import { AddItem } from "../../feautres/AddItem"

export const routes = [
  {
    path: "/",
    Component: LogIn,
  },
  {
    path: "/inventory",
    Component: Inventory,
  },
  {
    path: "/create",
    Component: AddItem
  }
]

export function Router({ routes = [], defaultComponent: DefaultComponent = () => <h1>404</h1> }) {
  const [currentPath, setcurrentPath] = useState(window.location.pathname)

  useEffect(() => {
    const changeLocation = () => { setcurrentPath(window.location.pathname) }

    window.addEventListener(NavigationEvents.navigate, changeLocation)
    window.addEventListener(NavigationEvents.popstate, changeLocation)

    return () => {
      window.removeEventListener(NavigationEvents.navigate, changeLocation)
      window.removeEventListener(NavigationEvents.popstate, changeLocation)
    }
  }, [])

  const Page = routes.find(({ path }) => path === currentPath)?.Component

  return Page ? <Page /> : <DefaultComponent />
}
