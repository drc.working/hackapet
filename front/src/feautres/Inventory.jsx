import { useEffect, useState } from "react"
import { requester } from "../domain/Requester.js"
import { GoogleLogOut } from "./GoogleLogOut.jsx"
import { ItemList } from "./ItemList.jsx"
import { AddItem } from "./AddItem.jsx"

export function Inventory() {
  const [info, setInfo] = useState({})

  useEffect(() => {
    const getUser = async () => {
      const url = 'http://localhost:8000/v1/users'
      const {name, sign} = await requester.get({url})

      setInfo({name, sign})
    }
    getUser()
  }, [])

  return (
    <main>
      <header><GoogleLogOut/></header>
      <h1>Inventory</h1>
      <p>Bienvenido a tu inventario {info.name}</p>
      <AddItem owner={info.sign}/>
      <ItemList/>
    </main>
  )
}